local scene = {}

function scene:enter()
    love.mouse.setRelativeMode(false)
end

function scene:draw()
    love.graphics.draw(images.title, 0, 0)
end

function scene:handleInput()
    if input:isPressed("click") then
        roomy:enter(scenes.gameplay)
    end
end

return scene
