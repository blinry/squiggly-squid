local Seaurchin = class("Seaurchin", PhysicsObject)

function Seaurchin:initialize(position)
    PhysicsObject.initialize(self, position.x, position.y, 120, 120, images.dark_seaurchin_purple)
    self.dangerous = true
end

return Seaurchin
