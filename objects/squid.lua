local Squid = class("Squid", PhysicsObject)

function Squid:initialize(position, angle, scale)
    self.base_image = images.squid_straight_shortest

    PhysicsObject.initialize(self, position.x, position.y, 50, 70, self.base_image)

    self.angle = angle or 0
    self.scale = scale or 1

    self.gravity = 200;
    self.gravityAir = 90000;

    -- random color (copied from fish class)
    self.color = {
        r = 0.5+0.5*love.math.random(),
        g = 0.5+0.5*love.math.random(),
        b = 0.5+0.5*love.math.random(),
    }

    -- stuff for straight swimming animation
    local swimAnimationFrames = {
        images.squid_straight_shorter,
        images.squid_straight_basic,
        images.squid_straight_longer,
        images.squid_straight_longer2,
        images.squid_straight_longest,
        images.squid_straight_longest,
        images.squid_straight_longest,
        images.squid_straight_longest,
        images.squid_straight_longer2,
        images.squid_straight_longer,
        images.squid_straight_basic,
        images.squid_straight_shorter,
        images.squid_straight_shortest,
    }
    self.swimAnimation = Animation:new(self, 0.3, swimAnimationFrames)

    -- stuff for left turn animation
    local tlAnimationFrames = {
        images.squid_left1,
        images.squid_left2,
        images.squid_left3,
        images.squid_left4,
        images.squid_left3,
        images.squid_left2,
        images.squid_left1,
        images.squid_straight_shortest,
    }
    self.tlAnimation = Animation:new(self, 0.3, tlAnimationFrames)

    -- stuff for right turn animation
    local trAnimationFrames = {
        images.squid_right1,
        images.squid_right2,
        images.squid_right3,
        images.squid_right4,
        images.squid_right3,
        images.squid_right2,
        images.squid_right1,
        images.squid_straight_shortest,
    }
    self.trAnimation = Animation:new(self, 0.3, trAnimationFrames)

    -- stuff for grabbing animation
    local grabAnimationFrames = {
        images.squid_grab1,
        images.squid_grab2,
        images.squid_grab3,
        images.squid_grab4,
        images.squid_grab5,
        images.squid_grab6,
        images.squid_grab7,
        images.squid_straight_shortest,
    }
    self.grabAnimation = Animation:new(self, 0.2, grabAnimationFrames)

    -- stuff for dropping animation
    local dropAnimationFrames = {
        images.squid_straight_shortest,
        images.squid_grab7,
        images.squid_grab6,
        images.squid_grab5,
        images.squid_grab4,
        images.squid_grab3,
        images.squid_grab2,
        images.squid_grab1,
        images.squid_straight_shortest,
    }
    self.dropAnimation = Animation:new(self, 0.2, dropAnimationFrames)


    self.world = world
    self.holding = {}

    self.prevRotation = 0 --for tracking changes of rotation
end

function Squid:materialize(world)
     -- don't call PhysicsObject:materialize(), instead do everything here, 
    -- so the squid can have a different body:
    
    self.body = love.physics.newBody(world, self:getPosition().x, self:getPosition().y, "dynamic")
    self.body:setUserData(self)
    local rectangleShape = love.physics.newRectangleShape(0, 0, self:getWidth(), self:getHeight())
    local circleShape1 = love.physics.newCircleShape(0, -self:getHeight()/2, self:getWidth()/2)
    love.physics.newFixture(self.body, rectangleShape)
    love.physics.newFixture(self.body, circleShape1)
    self.angularDamping = 5
    self.linearDamping = 2.5
    self.materialized = true
    self.world = world
end

function Squid:update(dt)
    PhysicsObject.update(self, dt)

    -- left turn animation:
    if input:isPressed("left") then
        self.tlAnimation:start()
        -- special case when above the water:
        if self:getPosition().y < 0 then
            self.body:applyLinearImpulse(-100,0)
        end
    end

    if input:isDown("left") then 
        self.tlAnimation:pauseOnFrame(4)        
    end

    if input:isReleased("left") then 
        self.tlAnimation:resume()
    end

    -- right turn animation:
    if input:isPressed("right") then
        self.trAnimation:start()
        -- special case when above the water:
        if self:getPosition().y < 0 then
            self.body:applyLinearImpulse(100,0)
        end
    end

    if input:isDown("right") then 
        self.trAnimation:pauseOnFrame(4)
    end

    if input:isReleased("right") then 
        self.trAnimation:resume()
    end

    -- drifting forwards/backwards:
    local velocity = vector(self.body:getLinearVelocity())
    local angle = vector(0, -1):rotated(self.body:getAngle())
    local drift = velocity:projectOn(angle)*dt*30
    self.body:applyForce(drift.x, drift.y)

    -- gravity:
    self.body:applyForce(0, 10000*dt)

    -- rotating upwards:
    --local angle = self.body:getAngle()
    --angle = angle % (2*math.pi)
    --local dir = 0
    --if angle < math.pi then
    --    dir = -1
    --else
    --    dir = 1
    --end
    --self.body:applyTorque(dt*100000*dir)

    -- turning physics:
    local rotationAmount = input:getX()
    self.body:applyTorque(dt*2500000*rotationAmount)
    -- animation according to turning physics:
    local pr = self.prevRotation
    if rotationAmount == pr and rotationAmount ~= 0 then
        --
    elseif rotationAmount > pr and rotationAmount > 0 then
        if self.trAnimation.frame == 0 then
            self.trAnimation:start()
        else
            self.trAnimation:pauseOnFrame(4)
        end
    elseif rotationAmount < pr and rotationAmount < 0 then
        if self.tlAnimation.frame == 0 then
            self.tlAnimation:start()
        else
            self.tlAnimation:pauseOnFrame(4)
        end
    elseif rotationAmount > pr and rotationAmount < 0 then
        self.tlAnimation:resume()
    elseif rotationAmount < pr and rotationAmount > 0 then
        self.trAnimation:resume()
    elseif rotationAmount == 0 and pr > 0 then
        self.trAnimation:resume()
    elseif rotationAmount == 0 and pr < 0 then
        self.tlAnimation:resume()
    end
    -- store current rotation for next update:
    self.prevRotation = rotationAmount

    if input:isPressed("swim") then
        -- start swim animation:
        self.swimAnimation:start()
        -- swimming physics:
        local angle = self.body:getAngle() - math.pi/2
        local forwardFactor = 350
        if self:getPosition().y < 0 then
            forwardFactor = 10
        end
        self.body:applyLinearImpulse(math.cos(angle)*forwardFactor, math.sin(angle)*forwardFactor)
        -- sfx:
        sounds.swim:setPitch(love.math.random(80, 120)/100)
        sounds.swim:play()
    end

    -- grabbing things
    if input:isPressed("grab") then
        if #self.holding > 0 then
            self:releaseEverything()
        else
            sounds.grab:setPitch(love.math.random(100, 120)/100)
            sounds.grab:play()

            local grabRay = function(dAngle)
                if #self.holding == 0 then
                    local x, y = self.body:getPosition()
                    local angle = self.body:getAngle() + math.pi/2 + dAngle
                    local length = 150
                    local x2 = x + math.cos(angle)*length
                    local y2 = y + math.sin(angle)*length
                    self.world:rayCast(x, y, x2, y2, function(fixture, rayX, rayY, xn, yn) 
                        local userData = fixture:getBody():getUserData()
                        if userData and not userData.dangerous then
                            local x3 = x + math.cos(angle - dAngle)*20
                            local y3 = y + math.sin(angle - dAngle)*20
                            local fishX, fishY = fixture:getBody():getPosition()

                            local joint = love.physics.newDistanceJoint(self.body, fixture:getBody(), x3, y3, rayX, rayY, false)
                            --joint:setFrequency(4)
                            --joint:setDampingRatio(10)
                            joint:setLength(0)
                            table.insert(self.holding, joint)

                            return 0
                        end
                        return -1
                    end)
                end
            end

            grabRay(0)
            grabRay(math.pi/4)
            grabRay(-math.pi/4)

            self.grabAnimation:start()
        end
    end

    self.swimAnimation:update(dt)
    self.tlAnimation:update(dt)
    self.trAnimation:update(dt)
    self.grabAnimation:update(dt)
    self.dropAnimation:update(dt)

end

function Squid:releaseEverything()
    local didRelease = false
    for i, joint in ipairs(self.holding) do
        if not joint:isDestroyed() then
            joint:destroy()
            didRelease = true
        end
    end
    self.dropAnimation:start()
    self.holding = {}
    if didRelease then
        sounds.release:setPitch(love.math.random(80, 120)/100)
        sounds.release:play()
    end
end

function Squid:draw()
    -- draw body:
    love.graphics.setColor(self.color.r, self.color.g, self.color.b)
    Object.draw(self)
    -- draw eyes:
    love.graphics.setColor(1, 1, 1)
    local image = self.image -- store current image
    self.image = images.squid_eyes -- insert eyes
    Object.draw(self)
    self.image = image -- restore image from before
end

function Squid:hurt(byWhat)
    Object.hurt(self, byWhat, 1000)
    self:releaseEverything()
    -- sound
    sounds.hurt:setPitch(love.math.random(80, 120)/100)
    sounds.hurt:play()
end

return Squid
